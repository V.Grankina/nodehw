import FileDB from "./fileDB";

const newspostSchema = {
  id: Number,
  title: String,
  text: String,
  createDate: Date,
};

FileDB.registerSchema("newspost", newspostSchema);
const newspostTable = FileDB.getTable("newspost");

async function test() {
  const newsposts = newspostTable.getAll();
  const data = {
    title: "У зоопарку Чернігова лисичка народила лисеня",
    text: "В Чернігівському заопарку сталася чудова подія! Лисичка на ім'я Руда народила чудове лисенятко! Тож поспішайте навідатись та подивитись на це миле створіння!",
  };
  const createdNewspost = await newspostTable.create(data);
  const createdNewspost1 = await newspostTable.create(data);
  const createdNewspost2 = await newspostTable.create(data);
  const createdNewspost3 = await newspostTable.create(data);
  console.log(createdNewspost2);

  const post = await newspostTable.getById(+createdNewspost2.id);
  console.log(post);

  const updatedNewsposts = await newspostTable.update(
    +createdNewspost2.id,
    { title: "Маленька лисичка", fake: "Im so fake" }
  );
  console.log(updatedNewsposts);

  const id = await newspostTable.delete(+createdNewspost2.id);
  console.log(id);
}
test();


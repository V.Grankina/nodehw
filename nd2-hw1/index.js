const http = require('http');

const hostname = 'localhost';


const arguments = process.argv.slice(2).reduce((acc, cur)=>{
    const [arg, val] = cur.split('=');
    acc[arg.replace('--', '')] = val;
    return acc;
}, {});

const port = arguments.port || 3000;

let counter = 0;
const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'application/json');
  res.end(
    JSON.stringify({
      message: 'Request handled successfully',
      requestCount: counter,
    }),
  );
  // eslint-disable-next-line no-plusplus
  counter++;
});
server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});

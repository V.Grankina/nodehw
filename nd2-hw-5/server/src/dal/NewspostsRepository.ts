import fileDB from "../fileDB";
import { Post, PagedPosts, UserDataForPost } from "../types/posts.interface";
import { Params } from "../types/params.interface";



export default class NewspostsRepository {
  private newspostTable = fileDB.getTable("newspost");

  async getAll(params: Params): Promise<PagedPosts> {
    
    const array = await this.newspostTable.getAll();;
    let result = array;
    const total = result.length;

    if (params.size != null && params.page != null) {
      result = result.splice(params.page * params.size, params.size);
    }

    return {
      result,
      total,
      size: params.size,
      page: params.page,
    }; 
  }
  async getById(id: number): Promise<Post | undefined> {
    return this.newspostTable.getById(id);
  }
  async create(data: UserDataForPost): Promise<Post> {
    return this.newspostTable.create(data);
  }
  async update(id: number, update: UserDataForPost): Promise<Post | undefined> {
    return this.newspostTable.update(id, update);
  }
  async delete(id: number): Promise<number> {
    return this.newspostTable.delete(id);
  }
}

import fileDB from "../fileDB";
import { Post, PagedPosts, UserDataForPost } from "../types/posts.interface";
import { Params } from "../types/params.interface";

import NewspostsRepository from "../dal/NewspostsRepository";


export default class NewspostsService {
  private newsPostRepository: NewspostsRepository;

  constructor(newsPostRepository: NewspostsRepository){
    this.newsPostRepository = newsPostRepository;
  }
  async getAll(params: Params): Promise<PagedPosts> {
    return this.newsPostRepository.getAll(params);
  }
  async getById(id: number): Promise<Post | undefined> {
    return this.newsPostRepository.getById(id);
  }
  async create(data: UserDataForPost): Promise<Post> {
    return this.newsPostRepository.create(data);
  }
  async update(id: number, update: UserDataForPost): Promise<Post | undefined> {
    return this.newsPostRepository.update(id, update);
  }
  async delete(id: number): Promise<number> {
    return this.newsPostRepository.delete(id);
  }
}

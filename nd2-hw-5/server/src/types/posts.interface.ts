export interface UserDataForPost {
  title: String,
    text: String,
}


export interface Post {
    id: number,
    title: String,
    text: String,
    createDate: Date,
  }
  
  export interface PagedPosts {
    result: Post[];
    total: number;
    size: number | null;
    page: number | null;
  }
  
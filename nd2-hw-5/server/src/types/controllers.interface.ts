import express from "express";

export default interface controller {
    path: string;
    router: express.Router;
    initializeRoutes(): void;
}
import { Table } from "./Table";

export interface Schema {
  [key: string]: StringConstructor | NumberConstructor | DateConstructor;
}

export class FileDB {
  private schemas: Record<string, Schema>;
  constructor() {
    this.schemas = {};
  }

  registerSchema(name: string, schema: Schema): void {
    this.schemas[name] = schema;
  }
  getTable(name: string): Table {
    if (!this.schemas[name]) {
      throw new Error(`Schema '${name}' not found.`);
    }
    return new Table(name, this.schemas[name]);
  }
}



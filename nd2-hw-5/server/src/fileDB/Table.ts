import fsp from "fs/promises";
import fs from "fs";
import { Schema } from "./FileDB";
import path from "path";
import { Post, UserDataForPost} from "../types/posts.interface";

export class Table {
  private name: string;
  private schema: Schema;
  private filePath: string;

  constructor(name: string, schema: Schema) {
    this.name = name;
    this.schema = schema;
    this.filePath = path.join('src', `${name}.json`);

    if (!fs.existsSync(this.filePath)) {
      fs.writeFileSync(this.filePath, JSON.stringify([]), "utf-8");
    } else {
      console.error("File already exist");
    }
  }

  async getAll(): Promise<Array<Post>> {
    const posts = await fsp.readFile(this.filePath, "utf-8");
    return JSON.parse(posts);
  }

  async getById(id: number): Promise<Post | undefined> {
    const posts = await this.getAll();
    const post = posts.find((value) => value.id === id);
    if (!post) {
      throw new Error(`Post with id '${id}' not found.`);
    }
    return post;
  }

  async create(data: UserDataForPost): Promise<Post> {
    const posts = await this.getAll();
    let id = posts.length === 0 ? 1 : +posts[posts.length-1].id+1;
    const createDate = new Date();
    const newPost = { id, createDate, ...data };
    posts.push(newPost);
    await fsp.writeFile(this.filePath, JSON.stringify(posts));
    id++;
    return newPost;
  }

  async update(id: number, data: UserDataForPost): Promise<Post | undefined> {
    const post = await this.getById(id);
    if (!post){
      return undefined;
    }
    const posts = await this.getAll();
    const updatedPost = { ...post, ...data };
    const updatedPosts = posts.map((value) =>
      value.id === id ? updatedPost : value
    );
    fsp.writeFile(this.filePath, JSON.stringify(updatedPosts));
    return updatedPost;
  }

  async delete(id: number): Promise<number> {
    const posts = await this.getAll();
    const postToDelete = posts.findIndex((value) => value.id === id);
    if (postToDelete === -1) {
      throw new Error(`Post with id '${id}' not found.`);
    }
    const updatedPosts = posts.filter((value) => value.id !== id);
    fsp.writeFile(this.filePath, JSON.stringify(updatedPosts));
    return id;
  }
}

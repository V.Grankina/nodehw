
import { Request, Response, NextFunction } from "express";


const errorHandle = (
  err: Error,
  req: Request,
  res: Response,
  next: NextFunction
) => {
  res.status(500).send(JSON.stringify(Error));
};

export default errorHandle;

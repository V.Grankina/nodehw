import express from "express";
import bodyParser from "body-parser";
import controller from "../types/controllers.interface";
import errorHandle from "./utils/errorHandle";

export default class App {
  public app: express.Application;
  public port: number;
  constructor(controllers: controller[], port: number) {
    this.app = express();
    this.port = port;

    this.initializeMiddlewares();
    this.initializeControllers(controllers);
    this.initializeErrorHandle();
  }
  initializeMiddlewares() {
    this.app.use(bodyParser.json());
  }
  initializeControllers(controllers: controller[]) {
    controllers.forEach((controller: controller) => {
      this.app.use(controller.path, controller.router);
    });
  }
  private initializeErrorHandle() {
    this.app.use(errorHandle);
  }

  public listen() {
    this.app.listen(this.port, () => {
      console.log(`App listening on the port ${this.port}`);
    });
  }
}

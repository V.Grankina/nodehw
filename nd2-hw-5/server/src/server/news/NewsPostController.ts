import NewspostsService from "../../bll/NewspostsService";
import { Service } from "typedi";
import express from "express";
import { Post } from "../../types/posts.interface";
import { UserDataForPost } from "../../types/posts.interface";
import controller from "../../types/controllers.interface";

export default class NewsPostControler implements controller {
  public path = "/api/newsposts";
  public router = express.Router();
  private newspostServices: NewspostsService;

  constructor(newsPostServices: NewspostsService) {
    this.newspostServices = newsPostServices;
    this.initializeRoutes();
  }
  public initializeRoutes() {
    this.router.get("/", this.getAll);
    this.router.get("/:id", this.getById);
    this.router.delete("/:id", this.delete);
    this.router.post("/", this.create);
    this.router.put("/:id", this.update);
  }

  getAll = async (request: express.Request, response: express.Response) => {
    const params = {
      size: request.query.size ? Number(request.query.size) : null,
      page: request.query.page ? Number(request.query.page) : null,
    };
    const pagedNewsPosts = await this.newspostServices.getAll(params);
    response.send(pagedNewsPosts);
  };

  getById = async (
    request: express.Request,
    response: express.Response,
    next: express.NextFunction
  ) => {
    try {
      const id = Number(request.params.id);
      console.log(id);
      const post = await this.newspostServices.getById(id);
      response.send(JSON.stringify(post));
    } catch (error) {
      if ((error as Error).message.includes("Post with id")) {
        response.status(404).send((error as Error).message);
      }
      next(error);
    }
  };

  delete = async (
    request: express.Request,
    response: express.Response,
    next: express.NextFunction
  ) => {
    try {
      const id = Number(request.params.id);
      const deletedId = await this.newspostServices.delete(id);
      response.send("Deleted product with id:" + deletedId);
    } catch (error) {
      if ((error as Error).message.includes("Post with id")) {
        response.status(404).send((error as Error).message);
      }
      next(error);
    }
  };

  create = async (
    request: express.Request,
    response: express.Response,
    next: express.NextFunction
  ) => {
    try {
      const newPost = await this.newspostServices.create(request.body);
      response.send(newPost);
    } catch (error) {
      next(error);
    }
  };

  update = async (
    request: express.Request,
    response: express.Response,
    next: express.NextFunction
  ) => {
    try {
      const id = +request.params.id;
      const data: UserDataForPost = request.body;
      const updated = await this.newspostServices.update(id, data);
      response.send(updated);
    } catch (error) {
      if ((error as Error).message.includes("Post with id")) {
        response.status(404).send((error as Error).message);
      }
      next(error);
    }
  };
}

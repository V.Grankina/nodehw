import App from "./server/App";
import NewsPostControler from "./server/news/NewsPostController";
import "reflect-metadata";
import NewspostsRepository from "./dal/NewspostsRepository";
import NewspostsService from "./bll/NewspostsService";

const repository = new NewspostsRepository();
const service = new NewspostsService(repository);
const controller = new NewsPostControler(service);

const app = new App(
[new NewsPostControler(new NewspostsService(new NewspostsRepository()))],
8080);
    

app.listen();

const spinKeyframes = `
  @keyframes spin {
    from {
      transform: rotate(0deg);
    }
    to {
      transform: rotate(360deg);
    }
  }
`;

const loaderStyle = {
  border: "4px solid #f3f3f3",
  borderTop: "4px solid #3498db",
  borderRadius: "50%",
  width: "40px",
  height: "40px",
  animation: "spin 2s linear infinite",
};

export default function Loader() {
  return (
    <div className="d-flex align-items-center justify-content-center vh-100">
      <style>{spinKeyframes}</style>
      <div style={loaderStyle}></div>
    </div>
  );
}

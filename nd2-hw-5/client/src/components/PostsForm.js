import React, { useState, useEffect } from "react";
import Button from "react-bootstrap/esm/Button";
import Form from "react-bootstrap/Form";

export default function NewsForm({ onSubmit, post }) {
  const [title, setTitle] = useState("");
  const [text, setText] = useState("");

  useEffect(() => {
    if (post) {
      setTitle(post.title);
      setText(post.text);
    }
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    onSubmit({ title, text });
  };

  return (
    <Form noValidate onSubmit={handleSubmit}>
      <Form.Group className="mb-3" controlId="title">
        <Form.Label>Post title</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter Title"
          value={title}
          onChange={(e) => setTitle(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="text">
        <Form.Label>Post text</Form.Label>
        <textarea
          className="form-control"
          id="text"
          name="text"
          value={text}
          onChange={(e) => setText(e.target.value)}
          placeholder="Enter a text"
          required
          rows="3"
        ></textarea>
      </Form.Group>
      <Button variant="primary" type="submit">
        Submit
      </Button>
    </Form>
  );
}

import Button from "react-bootstrap/Button";
import { Link } from "react-router-dom";
import Container from "react-bootstrap/Container";
import Post from "../components/Post";
import Loader from "../components/Loader";
import { Pagination } from "@mui/material";

export default function MainPage({
  posts,
  onPostDelete,
  isLoading,
  pageQty,
  page,
  setPage,
}) {
  return (
    <>
      {isLoading ? (
        <Loader />
      ) : (
        <Container style={{ paddingTop: 60 }} className="d-flex flex-column align-items-center">
          <Link to="/create">
            <Button variant="success" style={{ marginBottom: 60 }}>
              Add a new post
            </Button>
          </Link>
          {!!pageQty && (
            <Pagination
              count={pageQty}
              page={page}
              onChange={(_, num) => setPage(num)}
            />
          )}
          {posts.length === 0 ? (
            <div>"Posts is not found"</div>
          ) : (
            <>
              {posts.map((post) => {
                return (
                  <Post
                    key={post.id}
                    style={{ width: "30rem" }}
                    post={post}
                    onPostDelete={onPostDelete}
                  />
                );
              })}
            </>
          )}
        </Container>
      )}
    </>
  );
}

import React, { useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import PostsForm from "../components/PostsForm";
import axios from "axios";
import Loader from "../components/Loader";
import Container from "react-bootstrap/esm/Container";
import { Link } from "react-router-dom";

export default function EditPostPage({ onPostEdit }) {
  const [post, setPost] = useState("");
  const [isLoading, setIsLoading] = useState(true);
  const { postId } = useParams();
  const navigate = useNavigate();

  useEffect(() => {
    axios
      .get(`/api/newsposts/${postId}`)
      .then((response) => {
        setPost(response.data);
        setIsLoading(false);
      })
      .catch((error) => {
        console.log(error);
        navigate("/");
      });
  });

  const handleSubmit = ({ title, text }) => {
    onPostEdit(postId, title, text);
    navigate("/");
  };

  return (
    <div>
      {isLoading ? (
        <Loader />
      ) : (
        <Container style={{ paddingTop: 60 }}>
          <h2>Create a post</h2>
          <PostsForm onSubmit={handleSubmit} post={post} />
          <Link to="/">Go back to the main page</Link>
        </Container>
      )}
    </div>
  );
}

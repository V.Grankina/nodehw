import React, { useState, useEffect } from "react";
import { Route, Routes, useNavigate } from "react-router-dom";
import axios from "axios";
import NotFoundPage from "./pages/NotFoundPage";
import MainPage from "./pages/MainPage";
import AddNewPostPage from "./pages/AddNewPostPage";
import EditPostPage from "./pages/EditPostPage";
import PostPage from "./pages/PostPage";

const BASE_URL = `/api/newsposts/`;
const SIZE = 4;

function App() {
  const [posts, setPosts] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [page, setPage] = useState(1);
  const [pageQty, setPageQty] = useState(0);
  const navigate = useNavigate();

  useEffect(() => {
    axios
      .get(BASE_URL + `?size=${SIZE}&page=${page - 1}`)
      .then((response) => {
        setPosts(response.data.result);
        setIsLoading(false);
        setPageQty(response.data.total%SIZE === 0 ? (response.data.total / SIZE) : Math.floor(response.data.total / SIZE +1));
        console.log(response.data);
      })
      .catch((error) => console.log(error));
  }, [page, posts]);

  const handleDelete = (postId) => {
    axios
      .delete(`/api/newsposts/${postId}`)
      .then((response) => {
        setPosts(posts.filter(({ id }) => id !== postId));
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const handleCreate = ({ title, text }) => {
    const responseBody = {
      title,
      text,
    };
    axios
      .post(`/api/newsposts/`, responseBody)
      .then((response) => navigate("/"))
      .catch((error) => {
        console.log(error);
      });
  };

  const handleEdit = (postId, title, text) => {
    const responseBody = {
      title,
      text,
    };
    axios
      .put(`/api/newsposts/${postId}`, responseBody)
      .then((response) => {
        const updatedPost = response.data;
        setPosts((prevState) => {
          const newState = prevState.map((value) =>
            value.id === updatedPost.id ? updatedPost : value
          );
          return newState;
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };
  return (
    <Routes>
      <Route
        path="/"
        element={
          <MainPage
            pageQty={pageQty}
            setPageQty={setPageQty}
            page={page}
            setPage={setPage}
            posts={posts}
            onPostDelete={handleDelete}
            isLoading={isLoading}
          />
        }
      />
      <Route
        path="/:postId"
        element={<PostPage onPostDelete={handleDelete} />}
      />
      <Route
        path="/edit/:postId"
        element={<EditPostPage onPostEdit={handleEdit} />}
      />
      <Route
        path="/create"
        element={<AddNewPostPage onAddNewPost={handleCreate} />}
      />
      <Route path="*" element={<NotFoundPage />} />
    </Routes>
  );
}

export default App;

import express, { NextFunction, Request, Response } from "express";
import fileDB from "./fileDB";
import * as bodyParser from "body-parser";
import postsRoutes from "./routes/posts";

const app = express();

app.use(bodyParser.json());
app.use("/api/newsposts", postsRoutes);

app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
  res.status(500).send("Something is wrong");
});

export const newspostTable = fileDB.getTable("newspost");

app.listen({ host: "localhost", port: 8080 }, () => {
  console.log("Express server listening on port 8080!");
});

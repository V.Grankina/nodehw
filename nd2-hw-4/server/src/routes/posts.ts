import * as express from "express";
import { nextTick } from "process";
import { newspostTable } from "..";

const router = express.Router();

router.get("/", async (request, response, next) => {
  try {
    const posts = await newspostTable.getAll();
    response.send(JSON.stringify(posts));
  } catch (error) {
    next(error);
  }
});

router.get("/:id", async (request, response, next) => {
  try {
    const id = +request.params.id;
    const post = await newspostTable.getById(id);
    response.send(JSON.stringify(post));
  } catch (error) {
    if ((error as Error).message.includes("Post with id")) {
      response.status(404).send((error as Error).message);
    }
    next(error);
  }
});

router.post("/", async (request, response, next) => {
  try {
    const newPost = await newspostTable.create(request.body);
    response.send(newPost);
  } catch (error) {
    next(error);
  }
});

router.put("/:id", async (request, response, next) => {
  try {
    const id = +request.params.id;
    const data = request.body;
    const updated = await newspostTable.update(id, data);
    response.send(updated);
  } catch (error) {
    if ((error as Error).message.includes("Post with id")) {
      response.status(404).send((error as Error).message);
    }
    next(error);
  }
});

router.delete("/:id", async (request, response, next) => {
  try {
    const id = +request.params.id;
    const deletedId = await newspostTable.delete(id);
    response.send("Deleted product with id:" + deletedId);
  } catch (error) {
    if ((error as Error).message.includes("Post with id")) {
      response.status(404).send((error as Error).message);
    }
    next(error);
  }
});

export default router;

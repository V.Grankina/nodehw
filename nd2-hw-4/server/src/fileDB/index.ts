import { FileDB } from './FileDB';

const newspostSchema = {
  id: Number,
  title: String,
  text: String,
  createDate: Date,
};

const filedB = new FileDB();
filedB.registerSchema('newspost', newspostSchema);

export default filedB;

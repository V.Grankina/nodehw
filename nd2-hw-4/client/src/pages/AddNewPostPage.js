import Container from "react-bootstrap/Container";
import { Link } from "react-router-dom";
import PostsForm from "../components/PostsForm";

export default function AddNewPostPage({ onAddNewPost }) {
  return (
    <Container style={{ paddingTop: 60 }}>
      <h2>Create a post</h2>
      <PostsForm onSubmit={onAddNewPost} />
      <Link to="/">Go back to the main page</Link>
    </Container>
  );
}

import Button from "react-bootstrap/Button";
import { Link } from "react-router-dom";
import Container from "react-bootstrap/Container";
import Post from "../components/Post";
import Loader from "../components/Loader";

export default function MainPage({ posts, onPostDelete , isLoading}) {
  return (
    <>
      {isLoading ? (
        <Loader />
      ) : (
        <Container style={{ paddingTop: 60 }}>
          <Link to="/create">
            <Button variant="success" style={{ marginBottom: 60 }}>
              Add a new post
            </Button>
          </Link>
          {posts.length === 0 ? (<div>"Posts is not found"</div>) : (<>{posts.map((post) => {
            return (
              <Post
                key={post.id}
                style={{ width: "30rem" }}
                post={post}
                onPostDelete={onPostDelete}
              />
            );
          })
        }</>)
      }
         
        </Container>
      )}
    </>
  );
}

import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { Link } from "react-router-dom";
import axios from "axios";
import Button from "react-bootstrap/esm/Button";
import Container from "react-bootstrap/Container";

export default function PostPage({onPostDelete}) {
  const [post, setPost] = useState({title:"", text: ""});
  const { postId } = useParams();
  const navigate = useNavigate();

  useEffect(() => {
    axios
      .get(`/api/newsposts/${postId}`)
      .then((response) => {
        setPost(response.data)})
      .catch((error) => {
        console.log(error);
        navigate("*");
      });
  },[postId]);

  const handleDeletePost= (postId) => {
    onPostDelete(postId);
    navigate('/');
  }

  return (
    <Container>
        <div className="d-flex align-items-center justify-content-around mt-3">
          <Button variant="success" onClick={()=>navigate(`/edit/${post.id}`)}>
            Edit post
          </Button>
          <Button variant="danger" onClick={() => handleDeletePost(post.id)}>
            Delete
          </Button>
          <Link to="/" className="btn btn-primary text-light">
            Go Home
          </Link>
          </div>
      <div className="d-flex align-items-center justify-content-center">
        <div className="text-center">
          <h1 className="display-1 fw-bold">{post.title}</h1>
          <p className="fs-3">{post.text}</p>
        </div>
      </div>
    </Container>
  );
}

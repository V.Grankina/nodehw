import React, { useState, useEffect } from "react";
import { Route, Routes, useNavigate } from "react-router-dom";
import axios from "axios";
import NotFoundPage from "./pages/NotFoundPage";
import MainPage from "./pages/MainPage";
import AddNewPostPage from "./pages/AddNewPostPage";
import EditPostPage from "./pages/EditPostPage";
import PostPage from "./pages/PostPage";


function App() {
  const [posts, setPosts] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const navigate = useNavigate();

  useEffect(() => {
    axios.get(`/api/newsposts/`).then((response) => {
      setPosts(response.data);
      setIsLoading(false);
    }).catch(error => console.log(error));
  }, [posts]);

  const handleDelete = (postId) => {
    axios
      .delete(`/api/newsposts/${postId}`)
      .then((response) => {
        setPosts(posts.filter(({ id }) => id !== postId));
      })
      .catch((error) => {
        console.log(error)
      });
  };

  const handleCreate = ({ title, text }) => {
    const responseBody = {
      title,
      text,
    };
    axios
      .post(`/api/newsposts/`, responseBody)
      .then((response) => navigate("/"))
      .catch((error) => {
        console.log(error);
      });
  };

  const handleEdit = (postId, title, text) => {
    const responseBody = {
      title,
      text,
    };
    axios.put(`/api/newsposts/${postId}`, responseBody)
    .then((response) => {
      const updatedPost = response.data;
      setPosts((prevState) => {
        const newState = prevState.map((value) =>
          value.id === updatedPost.id ? updatedPost : value
        );
        return newState;
      });
    }).catch((error) => {
      console.log(error);
    });
  };
  return (
    <Routes>
      <Route
        path="/"
        element={<MainPage posts={posts} onPostDelete={handleDelete} isLoading={isLoading}/>}
      />
      <Route
        path="/:postId"
        element={<PostPage onPostDelete={handleDelete} />}
      />
      <Route
        path="/edit/:postId"
        element={<EditPostPage onPostEdit={handleEdit} />}
      />
      <Route
        path="/create"
        element={<AddNewPostPage onAddNewPost={handleCreate} />}
      />
      <Route path="*" element={<NotFoundPage />} />
    </Routes>
  );
}

export default App;

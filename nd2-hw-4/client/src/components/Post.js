import Card from "react-bootstrap/Card";
import { useNavigate } from "react-router-dom";

export default function Post({ post, onPostDelete }) {
  const navigate = useNavigate();
  const handleClick = (postId) => {
    navigate(`/${postId}`);
  };
  return (
    <Card key={post.id}  onClick={()=>handleClick(post.id)}>
      <Card.Body>
        <Card.Title>{post.title}</Card.Title>
        <p style={{display: '-webkit-box', WebkitLineClamp: 3, WebkitBoxOrient: 'vertical',  overflow: 'hidden', textOverflow: 'ellipsis'}}>{post.text}</p>
      </Card.Body>
    </Card>
  );
}
